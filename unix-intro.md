<!-- _class: title -->

<!-- footer: @stijnvbrande -->

<div class="front">
    <div class="top">
    <div class="logo"></div>
    <div class="author">Stijn Vanden Brande<br/>@stijnvbrande</div>
    </div>
    <div class="title">Introduction to UNIX<br/><br/></div>
    <div class="location"><span></span>January 14, 2021</div>
</div>

---

# UNIX Introduction

- Directory structure
- Shells
- Services
- Packaging
- Alias
- Man / Help

---

# UNIX - Directory structure

- `/`: the root directory
- `/bin`: essential user binaries
- `/dev`: device files
- `/etc`: Configuration Files
- `/home`: Home Folders
- `/lib`: Essential Shared Libraries
- `/opt`: Optional Packages
- `/proc`: Kernel & Process Files
- `/root`: Root Home Directory
- `/sbin`: System Administration Binaries
- `/tmp`: Temporary Files
- `/usr`: User Binaries & Read-Only Data
- `/var`: Variable Data Files

---

# UNIX - Shells

A shell is a computer program which exposes an operating system's services to a user or program.

There are [many flavors of shells](https://en.wikipedia.org/wiki/Comparison_of_command_shells):
- [Bash](https://www.gnu.org/software/bash/) default shell on most systems
- [Csh](https://www.grymoire.com/Unix/Csh.html)
- [Zsh](https://www.zsh.org/), with configuration framework [Oh My Zsh](https://ohmyz.sh/)
- ...

---

# UNIX - Services

- Provide functionality / socket
- [Systemd](https://systemd.io/):
  - Suite of basic building blocks
  - Provides a system and service manager
  - Runs as PID 1 and starts the rest of the system

<div class="prelegend">Systemd usage</div>

```bash
systemctl enable UNIT...                      Enable one or more units, run on boot
systemctl disable UNIT...                     Disable one or more units, do not run on boot
systemctl start UNIT...                       Start (activate) one or more units
systemctl stop UNIT...                        Stop (deactivate) one or more units
systemctl reload UNIT...                      Reload one or more units
systemctl restart UNIT...                     Start or restart one or more units
systemctl try-restart UNIT...                 Restart one or more units if active
systemctl reload-or-restart UNIT...           Reload one or more units if possible,
                                              otherwise start or restart
```

---

# UNIX - Packages

There is always a need to install packages.

- Most distro have their own package manager and format:
  - RHEL / CentOS: [Yum]() format [RPM]()
  - Debian / Ubuntu: [Apt]() format [DEB]()
  - ...
- A package is basically a ZIP file with metadata
- Does not keep track of files no longer included in  the new package version
- Configuration files can be specified, so they are not overwritten on package update
- Hooks for before / after; installation, update and removal

---

# Unix - Alias

The alias command instructs the shell to replace one string with another when executing commands.

```bash
alias ll='ls -la'

# https://gitlab.global.ingenico.com/ahub/puppet/modules/ah_microservice/-/blob/master/templates/ahub-aliases.sh.erb
alias config-unsafe='sudo -u ahub jq -rS . /etc/ah-evry-online/config.json'

# https://gitlab.global.ingenico.com/ahub/puppet/modules/ah_enix/-/blob/master/manifests/eyaml_key.pp#L33
alias eyaml-sandbox-ahub='EYAML_CONFIG=/etc/eyaml/gi/sandbox-ahub.yaml eyaml'
```
---

# UNIX - Man / Help

The only command you need to know is **man**, the system entry point to the user manual.

```bash
man man
man curl
```

Executables / scripts usually also provide help output with flags *-h / --help*

```bash
[.../script-tools/release-process]$  ./release_process.sh --help

Usage: ./release_process.sh {options}

Authentication:
  --username            | -u     : Username for authentication with Artifactory, Confluence and Jira
  --password            | -p     : Password for authentication with Artifactory, Confluence and Jira
...
```

---

# UNIX down the rabbit hole

If you want to go further down the rabbit hole, you can check out the following resources:
- [Linux skill challenge](https://github.com/snori74/linuxupskillchallenge)
- [SystemD](http://0pointer.de/blog/projects/systemd.html)
- ...

---

Questions?

<!-- *footer: -->
