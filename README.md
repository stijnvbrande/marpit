# How to run with docker:

1. Start the container:
```bash
docker run --rm -it --init -v $PWD:/home/marp/app -e LANG=$LANG -p 8080:8080 -p 37717:37717 --entrypoint sh marpteam/marp-cli
```
2. Start marp-cli in the container:
```bash
node /home/marp/.cli/marp-cli.js --server --html --theme ~/app/inuits-169.css .
```
3. Open presentation in a browser: [http://localhost:8080/](http://localhost:8080/)
